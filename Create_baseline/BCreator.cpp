/*=================================================================================================
 * N-RMiner version 1.0 - Software to mine interesting Maximal Complete Connected Subsets (N-MCCSs)
 * from multi-relational data containing N-ary relationships.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * N-RMiner-1.0 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *================================================================================================*/

/** DatabaseGenerator.cpp
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/

#include <dirent.h>
#include <iostream>
#include <errno.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <sstream>
#include <cstring>

using namespace std;

typedef struct queue_el {
    unsigned int id;
    int start;
    int end;
}Qel;

void populate(unsigned int entity, unsigned int entity_type, set<unsigned int> ans, map<unsigned int, set<unsigned int> >* Anscestors, unsigned int h, unsigned int maxheight, map<unsigned int, set<unsigned int> >& ientities, map<unsigned int, set<unsigned int> >& isentities, map<unsigned int, string>* EntityIdToEntityName, vector<set<unsigned int> >& Comp){
    set<unsigned int>::const_iterator it;
    unsigned int newh;
    for (it = ans.begin(); it!=ans.end(); it++){
        newh=h;
        isentities[entity].insert(*it);
        ientities[*it].insert(entity);
        Comp[entity_type].insert(*it);
        newh++;
        if (newh<=maxheight)
            populate(entity, entity_type, (*Anscestors)[*it],Anscestors,newh,maxheight,ientities,isentities, EntityIdToEntityName, Comp);
    }
    return;
}

//takes as input the set of numeric values - since it is a set they are ordered
void construct_partial_order(set<double> numeric_values, string type, unsigned int type_id, unsigned int& last_index, int start, int end, unsigned int height, map<unsigned int, string>& EntityIdToEntityName, map<string, unsigned int>& EntityValueToEntityId, map<unsigned int, set<unsigned int> >& Descendants, map<unsigned int, set<unsigned int> >& Anscestors, vector<set<unsigned int> >& Comp, map<unsigned int, unsigned int>& EntityToType, map<unsigned int, set<unsigned int> >& imp_entities, map<unsigned int, set<unsigned int> >& imp_sup_entities, fstream& out_one_file){
    
    //initiallisation
    int nstart = start;
    int nend = end;
    
    set<double>::iterator ti;
    set<unsigned int>::const_iterator ci;
    
    queue<Qel> Q;
    set<double>::iterator it1, it2;
    it1 = numeric_values.begin();
    advance(it1,start);
    it2 = numeric_values.begin();
    advance(it2,end);
    
    
    //int id = last_index;
    
    int cid1, cid2;
    
    stringstream parent_stream;
    parent_stream<<type<<".["<<*it1<<" "<<*it2<<"]";
    string parent = parent_stream.str();
    EntityIdToEntityName[last_index]=parent;
    //Comp[type_id].insert(last_index);
    Qel qi;
    qi.id = last_index;
    //out_one_file<<qi.id<<","<<"["<<*it1<<" "<<*it2<<"]"<<endl;
    EntityToType[last_index]=type_id;
    last_index++;
    qi.start = start;
    qi.end = end;
    Q.push(qi);
    
    while (!Q.empty()) {
        
        Qel pid_el;
        pid_el = Q.front();
        Q.pop();
        nstart = pid_el.start;
        nend = pid_el.end;
        
        it1 = numeric_values.begin();
        advance(it1,nstart);
        it2 = numeric_values.begin();
        advance(it2,nend);
    
        if (nstart!=nend-1){
            
            stringstream child1_stream;
            child1_stream<<type<<".["<<*next(it1)<<" "<<(*it2)<<"]";
            string child1 = child1_stream.str();
            if (EntityValueToEntityId.find(child1)!=EntityValueToEntityId.end())
                cid1 = EntityValueToEntityId[child1];
            else {
                cid1 = last_index;
                EntityToType[last_index]=type_id;
                EntityIdToEntityName[cid1]=child1;
                EntityValueToEntityId[child1]=cid1;
                //Comp[type_id].insert(last_index);
                Qel cid_el;
                cid_el.id = cid1;
                //out_one_file<<cid1<<","<<"["<<*next(it1)<<" "<<*it2<<"]"<<endl;
                cid_el.start = nstart+1;
                cid_el.end  = nend;
                Q.push(cid_el);
                last_index++;
                
            }
            Anscestors[cid1].insert(pid_el.id);
            Descendants[pid_el.id].insert(cid1);
            
            
            
            stringstream child2_stream;
            child2_stream<<type<<".["<<(*it1)<<" "<<*prev(it2)<<"]";
            string child2 = child2_stream.str();
            if (EntityValueToEntityId.find(child2)!=EntityValueToEntityId.end())
                cid2 = EntityValueToEntityId[child2];
            else {
                cid2 = last_index;
                EntityToType[last_index]=type_id;
                EntityIdToEntityName[cid2]=child2;
                EntityValueToEntityId[child2]=cid2;
                //Comp[type_id].insert(last_index);
                Qel cid_el;
                cid_el.id = cid2;
                //out_one_file<<cid2<<","<<"["<<*it1<<" "<<*prev(it2)<<"]"<<endl;
                cid_el.start = nstart;
                cid_el.end  = nend-1;
                Q.push(cid_el);
                last_index++;
               
            }
            Anscestors[cid2].insert(pid_el.id);
            Descendants[pid_el.id].insert(cid2);
            
        }
        else {
            stringstream child1_stream;
            child1_stream<<type<<"."<<(*it1);
            string child1 = child1_stream.str();
            if (EntityValueToEntityId.find(child1)!=EntityValueToEntityId.end()){
                cid1 = EntityValueToEntityId[child1];
                //if (Anscestors[cid1].size()==0)
                  //      out_one_file<<cid1<<","<<*it1<<endl;
                Anscestors[cid1].insert(pid_el.id);
                Descendants[pid_el.id].insert(cid1);
                
                populate(cid1, type_id, Anscestors[cid1], &Anscestors, 0,height, imp_entities, imp_sup_entities, &EntityIdToEntityName, Comp);
                /*set<unsigned int> up_set;
                set<unsigned int>::const_iterator ci;
                set<unsigned int>::const_iterator uit;
                
                up_set.insert(pid_el.id);
                imp_sup_entities[cid1].insert(pid_el.id);
                
                while (!up_set.empty()){
                    
                    uit=up_set.begin();
                    imp_entities[*uit].insert(cid1);
                    for (ci=Anscestors[*uit].begin(); ci!=Anscestors[*uit].end(); ci++){
                        up_set.insert(*ci);
                        imp_sup_entities[cid1].insert(*ci);
                    }
                    up_set.erase(*uit);
                }*/
            }
            
            stringstream child2_stream;
            child2_stream<<type<<"."<<(*it2);
            string child2 = child2_stream.str();
            if (EntityValueToEntityId.find(child2)!=EntityValueToEntityId.end()){
                cid2 = EntityValueToEntityId[child2];
                //if (Anscestors[cid2].size()==0)
                  //  out_one_file<<cid2<<","<<*it2<<endl;
                Anscestors[cid2].insert(pid_el.id);
                Descendants[pid_el.id].insert(cid2);
                
                populate(cid2, type_id, Anscestors[cid2], &Anscestors, 0,height, imp_entities, imp_sup_entities, &EntityIdToEntityName, Comp);
                /*set<unsigned int> up_set;
                set<unsigned int>::const_iterator ci;
                set<unsigned int>::const_iterator uit;
                
                up_set.insert(pid_el.id);
                imp_sup_entities[cid2].insert(pid_el.id);
                
                
                unsigned int h=1;
                unsigned int num_of_elements_per_level=0;
                
                while (!up_set.empty()){
                    
                    uit=up_set.begin();
                    imp_entities[*uit].insert(cid2);
                    for (ci=Anscestors[*uit].begin(); ci!=Anscestors[*uit].end() && h<height; ci++){
                        up_set.insert(*ci);
                        imp_sup_entities[cid2].insert(*ci);
                    }
                    
                    up_set.erase(*uit);
                }*/
            }
        
        }
    }
    numeric_values.clear();
    
    return;
}

void print_combs(vector<set<unsigned int> >* combs, unsigned int depth, fstream& out_nary_file, vector<unsigned int> sol){
    set<unsigned int>::const_iterator sit;
    if (sol.size()==(*combs).size()){
        for (unsigned int i=0; i<sol.size()-1; i++){
            out_nary_file<<sol[i]<<",";
        }
        out_nary_file<<sol[sol.size()-1]<<endl;
    }
    else {
        for (sit=(*combs)[depth].begin(); sit!=(*combs)[depth].end(); sit++){
            vector<unsigned int> nsol = sol;
            nsol.push_back(*sit);
            unsigned int ndepth = depth+1;
            print_combs(combs, ndepth, out_nary_file, nsol);
        }
    }
    return;
}

int main(int argc, char **argv) {

	string line;
	string type;
    string field;
	int argnumber=0;

    string baseline_o_dir;
    string baseline_n_dir;
    
	vector<string> OnetoOne;
	vector<string> Nary;
	vector<unsigned int> constraints;
	vector<unsigned int> read_constraints;

	DIR *dp;
	struct dirent *dirp;

	vector<vector<unsigned int> > RelInstIdToEntities; 			  //linking relationship instance ids to entities ids
	vector<vector<unsigned int> > RelsToTypes;					  //linking relationship types to the participating entity types
	vector<vector<vector<unsigned int> > > RelInstList;           //relationship instance ids for every entity and relationship type
	vector<vector<set<unsigned int> > > EntityAugList;            //for every entity, it stores the valid augmentation elements (or the entities it
                                                                  //is related to) in a different set for every entity type
    map<unsigned int, set<unsigned int> > Descendants;                         //for every non-categorical entity all the descendants of the hierarchy
    map<unsigned int, set<unsigned int> > Anscestors;                          //for every non-categorical entity all the anscestors of the hierarchy
    map<unsigned int, set<unsigned int> > implied_entities;
    map<unsigned int, set<unsigned int> > implied_sup_entities;
    
	vector<map<unsigned int,unsigned int> > EntityIDToUniqueID;  //structure used to give ids to entities in terms of increasing number of
                                                                  //relationship instances they participate in
	map<unsigned int, unsigned int> EntityToType;                            //linking every entity to its type
	//vector<vector<unsigned int> > EntityTypeToRelTypes;           //linking every entity type to the relationship types it participates
    
    
	map<unsigned int, unsigned int> EntityToModelIndex;                      //linking entity ids to their index in the MaxEnt model of the data
	map<unsigned int,string> EntityIdToEntityName;            //linking entity ids to the actual string they correspond to ex: film title, director name etc
    map<unsigned int,double> EntityIdToEntityValue;            //linking entity ids to the actual value they correspond to (only for numeric)
    
    map<string, unsigned int> EntityValueToEntityId;        //from value (as string) to Id (only for numeric)
	map<unsigned int, unsigned int> numOfEntitiesPerType;
	map<unsigned int, unsigned int> numOfRelInstsPerType;
	map<string, unsigned int> typeToTypeID;

    //initialisations
	int entityID = 0;
    unsigned int numOfEntities = 0;
	unsigned int entityIndex = 0;
	unsigned int relInstIndex = 0;
	unsigned int relInstID = 0;
	unsigned int entityType = 0;
    unsigned int reltype=0;
	double numOfPossibleRelInsts = 0;
    unsigned int first_index = 0;
    unsigned int last_index = 0;

	vector<set<unsigned int> > C;							  //initial solution C
	vector<set<unsigned int> > Comp;						  //Compatible elements initialisation (i.e. all entities) (per type)
	vector<set<unsigned int> > RelInsts_C;					  //Relationship instance ids in the set C
	vector<set<unsigned int> > RelInsts_Comp;                 //Relationship instance ids in the set Comp
	vector<bool> activeRelTypes;							  //boolean vector over all relationship types containing 1 if there is at least one
                                                              //entity from an entity type participating in the relationship type
	vector<bool> activeEntityTypes;							  //boolean vector over all entity types containing 1 if the entity type is related
                                                              //to at least one entity type already in the solution, 0 otherwise
    
    vector<char> typeOfType;                                 //specifies whether an entity type is hierarchy
                                                            //to be used when constructing partial orders
    vector<unsigned int> height;                           //has a 10^6 element of an entity type that is not a partial order or is a partial order for which we consider the whole height of the tree or >0 element to specify a specific tree height to consider about a partial order.
    map<unsigned int, char> typeIDToTypeofType;

	vector<map <unsigned int, int> >::const_iterator tit;
	map <unsigned int, int>::const_iterator nit;
    
    bool compute_interestingness=true;
    int num_to_print=200;
    bool read_nary=false;

    FILE* fp;
    char file[1000];
    char lineo[1000];
	fp = fopen(argv[1], "r");
    char* p;
    char* q;
    char* m;
    char* l;
    
    if (fp==NULL){
        fprintf(stderr, "Can't read %s\n", argv[1]);
        exit(1);
    }
	while (fgets(lineo, 1000, fp)!=NULL){ //read the config file
		argnumber++;
     
        l=strchr(lineo, ' ');
        q=strchr(lineo, '\r');
        m=strchr(lineo, '\n');
        p=lineo;
        int i=0;
        while (p!=l && p!=q && p!=m){
            file[i]=*p;
            i++;
            p++;
        }
        file[i]='\0';
        
        if ((dp  = opendir(file)) == NULL){ //one_to_one file
            typeOfType.push_back(*p);
            FILE * pFile;
            pFile = fopen (file,"r");
            if (pFile==NULL){
                cout<<"Error opening file: "<<lineo<<"!!!"<<endl;
                return errno;
            }
            fclose(pFile);
            cout<<"read one_to_one: "<<file<<endl;
            OnetoOne.push_back(file);
        }
        else {
            
            if (!read_nary){
                
                read_nary = true;
                cout<<"read nary dir: "<<file<<endl;
                while ((dirp = readdir(dp)) != NULL) {
                    
                    if(string(dirp->d_name)==string(".") || string(dirp->d_name)==string("..") || string(dirp->d_name)==string(".svn"))
                        continue;
                    Nary.push_back(string(file)+string(dirp->d_name));
                    //init
                    activeRelTypes.push_back(false);
                    set<unsigned int> tempt;
                    RelInsts_Comp.push_back(tempt);
                    RelInsts_C.push_back(tempt);
                }
                closedir(dp);
            }
            else {
                cout<<"read baseline dir: "<<file<<endl;
                closedir(dp);
                baseline_o_dir=file;
                baseline_n_dir=file;
                baseline_o_dir.append("one_to_one");
                baseline_n_dir.append("nary");
            }
        }
        //}
    }
    
    for (unsigned int i=0; i<OnetoOne.size(); i++){
        
         if (typeOfType[i]!='h'){
            map<unsigned int,unsigned int> temp;
            set<unsigned int> temps;
            vector<unsigned int> tempv;
            EntityIDToUniqueID.push_back(temp);
            activeEntityTypes.push_back(false);
            //EntityTypeToRelTypes.push_back(tempv);
            Comp.push_back(temps);
            C.push_back(temps);
            
            fstream onestream(OnetoOne[i].c_str(),ios::in);
            getline(onestream,line);
            
            if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                line.erase(line.length()-1);
            
            stringstream line_stream(line);
            getline(line_stream, type, ','); //gets the id
            getline(line_stream, type, ','); //gets the type
            
            if (typeToTypeID.find(type)==typeToTypeID.end()){
                typeToTypeID[type]=entityType;
                typeIDToTypeofType[entityType]=typeOfType[i];
                entityType++;
            }
         }
    }
    
	//process the relationship files first to get the number of relationship instances for every entity and sort the entities based on it
    unsigned int nodes=0;
	for (unsigned int b=0; b<Nary.size(); b++){
        
		vector<unsigned int> types;
        vector<string> type_names;
		unsigned int i;
        
        reltype++;

		fstream narystream(Nary[b].c_str(),ios::in);
		getline(narystream, line);
		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
					line.erase(line.length()-1);

		stringstream line_stream(line);
		while (getline(line_stream, type, ',')){

			types.push_back(typeToTypeID[type]);
            type_names.push_back(type);
            if (numOfEntitiesPerType.find(typeToTypeID[type])==numOfEntitiesPerType.end())
                numOfEntitiesPerType[typeToTypeID[type]]=0;
		}
        
		while(getline(narystream, line)){
			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);
			stringstream line_stream(line);
			i=0;
            
			while (getline(line_stream, field, ',')){
                
                if (EntityIDToUniqueID[types[i]].find(atoi(field.c_str()))==EntityIDToUniqueID[types[i]].end()){
                    
                    EntityIDToUniqueID[types[i]][atoi(field.c_str())]=nodes;
                    nodes++;
                    numOfEntitiesPerType[types[i]]=numOfEntitiesPerType[types[i]]+1;
                    numOfEntities++;
                }
                
				i++;
			}
		}
		types.clear();
	}

 
    
    //find index where to start the ids of single entities (interval entities should always have smaller ids because they need to be entered first)
    for (unsigned int i=0; i<OnetoOne.size(); i++){
        
        if (typeOfType[i]=='o'){
            unsigned int num = (EntityIDToUniqueID[i].size()*(EntityIDToUniqueID[i].size()-1))/2;
            first_index+=num;
        }
        else if (typeOfType[i]=='t'){
            unsigned int num = (EntityIDToUniqueID[i].size()*(EntityIDToUniqueID[i].size()-1))-1;
            first_index+=num;
        }
        else {
            
        }
    }
    
    //read files defining a hierarchy
    for (unsigned int i=0; i<OnetoOne.size(); i++){
        if (typeOfType[i]=='h'){
            fstream onestream(OnetoOne[i].c_str(),ios::in);
            
            getline(onestream,line);
            
            if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                line.erase(line.length()-1);
            
            stringstream line_stream(line);
            getline(line_stream, type, ','); //gets the type
            getline(line_stream, type, ','); //gets the type
            
            
            char delim;
            string id1, id2;
            string name;
            unsigned int idn1, idn2;
            while(getline(onestream,line)){
                
                if (line.find('\t')!=string::npos)
                    delim='\t';
                else
                    delim=',';
                
                if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                    line.erase(line.length()-1);
                
                stringstream line_stream(line);
                getline(line_stream, id1, delim);
                getline(line_stream, id2, delim);
                
                idn1 = atoi(id1.c_str());
                idn2 = atoi(id2.c_str());
                
                //if it does not already exist (entities at the lowest level have already an id)
                if (EntityIDToUniqueID[typeToTypeID[type]].find(idn1)==EntityIDToUniqueID[typeToTypeID[type]].end()){
                    EntityIDToUniqueID[typeToTypeID[type]][idn1]=first_index;
                    EntityToType[first_index]=typeToTypeID[type];
                    Comp[typeToTypeID[type]].insert(first_index);
                    first_index++;
                }
                if (EntityIDToUniqueID[typeToTypeID[type]].find(idn2)==EntityIDToUniqueID[typeToTypeID[type]].end()){
                    EntityIDToUniqueID[typeToTypeID[type]][idn2]=first_index;
                    EntityToType[first_index]=typeToTypeID[type];
                    Comp[typeToTypeID[type]].insert(first_index);
                    first_index++;
                }
             }
        }
    }
    
    //read the one_to_one files and assign a string to every entity id
	for (unsigned int a=0; a<OnetoOne.size(); a++){
       	if (typeOfType[a]!='h'){
        
        set<double> numeric_values; //to be filled only in the case of numeric entity types
        
        unsigned found = OnetoOne[a].find_last_of("/");
        string file = OnetoOne[a].substr(found);
        string base_file = baseline_o_dir+file;
        
		fstream out_one_file(base_file.c_str(),ios::out);
        
		fstream onestream(OnetoOne[a].c_str(),ios::in);
		getline(onestream,line);
        out_one_file<<line<<endl;

		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                	line.erase(line.length()-1);

		stringstream line_stream(line);
		getline(line_stream, type, ','); //gets the id
		getline(line_stream, type, ','); //gets the type
		
		entityType=typeToTypeID[type];
        
		
		char delim;
		string id;
		string name;
		unsigned int idn;

		while(getline(onestream,line)){
            
			if (line.find('\t')!=string::npos)
				delim='\t';
			else
				delim=',';

			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);

			stringstream line_stream(line);
			getline(line_stream, id, delim);
			getline(line_stream, name, delim);

			idn = atoi(id.c_str());
			if (EntityIDToUniqueID[entityType].find(idn)!=EntityIDToUniqueID[entityType].end()){
				entityID=EntityIDToUniqueID[entityType][idn];
				EntityIdToEntityName[entityID]=type+"."+name;
				Comp[entityType].insert(entityID);
                
                if (typeIDToTypeofType[entityType]=='o'){
                    EntityValueToEntityId[type+"."+name]=entityID;
                    numeric_values.insert(stod(name));
                   
                }
                
                if (typeIDToTypeofType[entityType]!='o')
                    out_one_file<<entityID<<","<<name<<endl;
                
				
			}
		}
		numOfRelInstsPerType[entityType]=0;
        
        if (typeIDToTypeofType[entityType]=='o'){
           
            construct_partial_order(numeric_values, type, entityType, last_index, 0, numeric_values.size()-1, height[a], EntityIdToEntityName, EntityValueToEntityId, Descendants, Anscestors, Comp, EntityToType, implied_entities, implied_sup_entities, out_one_file);
            
            set<unsigned int>::const_iterator cit, ait;
            bool delet;
            for (cit=Comp[entityType].begin(); cit!=Comp[entityType].end(); cit++){
                out_one_file<<*cit<<","<<EntityIdToEntityName[*cit]<<endl;
                delet = false;
                for (ait=Anscestors[*cit].begin(); ait!=Anscestors[*cit].end(); ait++){
                    if (Comp[entityType].find(*ait)==Comp[entityType].end()){
                        delet=true;
                        break;

                    }
                }
                if (delet){
                    set<unsigned int> temp;
                    Anscestors[*cit]=temp;
                }
            }
            
           //end of test
        }
        out_one_file.close();
        }
    }
    
    for (unsigned int i=0; i<OnetoOne.size(); i++){
        if (typeOfType[i]=='h'){
            fstream onestream(OnetoOne[i].c_str(),ios::in);
            
            getline(onestream,line);
            
            if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                line.erase(line.length()-1);
            
            stringstream line_stream(line);
            getline(line_stream, type, ','); //gets the type
            getline(line_stream, type, ','); //gets the type
            
            char delim;
            string id1, id2;
            string name;
            unsigned int idn1, idn2;
            while(getline(onestream,line)){
                
                if (line.find('\t')!=string::npos)
                    delim='\t';
                else
                    delim=',';
                
                if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                    line.erase(line.length()-1);
                
                stringstream line_stream(line);
                getline(line_stream, id1, delim);
                getline(line_stream, id2, delim);
                
                idn1 = atoi(id1.c_str());
                idn2 = atoi(id2.c_str());
                
                unsigned int parent = EntityIDToUniqueID[typeToTypeID[type]][idn1];
                unsigned int child = EntityIDToUniqueID[typeToTypeID[type]][idn2];
                //cout<<parent<<","<<child<<endl;
                Anscestors[child].insert(parent);
                Descendants[parent].insert(child);
                
            }
            
            set<unsigned int>::const_iterator cit;
            set<unsigned int> up_set;
            set<unsigned int>::const_iterator ci;
            set<unsigned int>::iterator uit;
            
            
            for (cit=Comp[typeToTypeID[type]].begin(); cit!=Comp[typeToTypeID[type]].end(); cit++){
                if (*cit>=first_index){
                    for (ci=Anscestors[*cit].begin(); ci!=Anscestors[*cit].end(); ci++){
                        up_set.insert(*ci);
                        implied_entities[*ci].insert(*cit);
                        implied_sup_entities[*cit].insert(*ci);
                    }
                    
                    while (!up_set.empty()){
                        
                        uit=up_set.begin();
                        
                        if (Anscestors.find(*uit)!=Anscestors.end()){
                            //cout<<EntityIdToEntityName[*uit]<<*uit<<endl;
                            //cout<<Anscestors[*uit].size()<<endl;
                            for (ci=Anscestors[*uit].begin(); ci!=Anscestors[*uit].end(); ci++){
                                //cout<<EntityIdToEntityName[*ci]<<" ";
                                up_set.insert(*ci);
                                implied_entities[*ci].insert(*cit);
                                implied_sup_entities[*cit].insert(*ci);
                            }
                            //cout<<endl;
                        }
                        up_set.erase(*uit);
                    }
                    up_set.clear();
                }
            }
        }
        
    }
    

	//Read again the relationship files to initialise structures, assign rel instance ids to entities and compute the number of rel instances for every entity in order to buid the MaxEnt model
    reltype=0;
	for (unsigned int b=0; b<Nary.size(); b++){
		
		relInstIndex = 0;
		vector<vector<int> > sumsdim;           //model marginals
		vector<unsigned int> typeIDs;
        vector<string> types;
        
        unsigned found = Nary[b].find_last_of("/");
        string file = Nary[b].substr(found);
        string base_file = baseline_n_dir+file;
        
        fstream out_nary_file(base_file.c_str(),ios::out);

		fstream narystream(Nary[b].c_str(),ios::in);
		getline(narystream, line);
        
        out_nary_file<<line<<endl;
        vector<set<unsigned int> > combs;
        
		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
			line.erase(line.length()-1);

		stringstream line_stream(line);

		int relation_dim=0;
		double num=1;

		while (getline(line_stream, type, ',')){
            
			relation_dim++;
			num = num * numOfEntitiesPerType[typeToTypeID[type]];
			//associate every type with the relationship
			typeIDs.push_back(typeToTypeID[type]);
            
			//EntityTypeToRelTypes[typeToTypeID[type]].push_back(reltype);
			//initialise marginals for Null model
            types.push_back(type);
			vector<int> temp;
            
			for (unsigned int i=0; i<numOfEntitiesPerType[typeToTypeID[type]]; i++){
				temp.push_back(0);
			}
			sumsdim.push_back(temp);
			temp.clear();
		}
		RelsToTypes.push_back(typeIDs);
		
		numOfPossibleRelInsts+=num;
        
        

		unsigned int i;
		unsigned int node=0;
		vector<unsigned int> fields;
		vector<unsigned int> newfields;
		string edge;
		while(getline(narystream, line)){
            
            for (int t=0; t<relation_dim; t++){
                set<unsigned int> combs_i;
                combs.push_back(combs_i);
            }
            
			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);
			stringstream line_stream(line);

			i=0;
			while (getline(line_stream, field, ',')){
				node=EntityIDToUniqueID[RelsToTypes[reltype][i]][atoi(field.c_str())];
				fields.push_back(node);
				sumsdim[i][EntityToModelIndex[node]]=sumsdim[i][EntityToModelIndex[node]]+1;
				i++;
			}
            
			relInstID++;
			relInstIndex++;
			RelInstIdToEntities.push_back(fields);
			RelInsts_Comp[reltype].insert(relInstID);

			for (int t=0; t<relation_dim; t++){
                
				RelInstList[fields[t]][reltype].push_back(relInstID);
                combs[t].insert(fields[t]);
                
                if (typeIDToTypeofType[typeIDs[t]]=='o' || typeIDToTypeofType[typeIDs[t]]=='p'){ /// for all anscestors add the relinst
                  
                    set<unsigned int>::const_iterator eid;
                    for (eid=implied_sup_entities[fields[t]].begin(); eid!=implied_sup_entities[fields[t]].end(); eid++){
                        
                        
                        RelInstList[*eid][reltype].push_back(relInstID);
                    
                        combs[t].insert(*eid);
                    }
                    
                    
                }
				for (int f=0; f<relation_dim; f++){
                    
					if (f!=t){
						EntityAugList[fields[t]][typeIDs[f]].insert(fields[f]);
                        if (typeIDToTypeofType[typeIDs[t]]=='o' || typeIDToTypeofType[typeIDs[t]]=='p'){
                            set<unsigned int>::const_iterator eid;
                            for (eid=implied_sup_entities[fields[t]].begin(); eid!=implied_sup_entities[fields[t]].end(); eid++){
                                
                                EntityAugList[*eid][typeIDs[f]].insert(fields[f]);
                            }
                            
                        }
                        if (typeIDToTypeofType[typeIDs[f]]=='o' || typeIDToTypeofType[typeIDs[f]]=='p'){
                            set<unsigned int>::const_iterator eid;
                            for (eid=implied_sup_entities[fields[f]].begin(); eid!=implied_sup_entities[fields[f]].end(); eid++){
                                EntityAugList[fields[t]][typeIDs[f]].insert(*eid);
                            }
                        }
                    }
				}
			}
            vector<unsigned int> sol;
            
            print_combs(&combs, 0, out_nary_file, sol);
            
            fields.clear();
            combs.clear();
        }
        
		reltype++;
        sumsdim.clear();
        out_nary_file.close();
    
    }
    
    RelInstIdToEntities.clear();
    RelsToTypes.clear();
    RelInstList.clear();
    EntityAugList.clear();
    EntityToType.clear();
    //EntityTypeToRelTypes.clear();
    EntityToModelIndex.clear();
    EntityIdToEntityName.clear();
    Descendants.clear();
    Anscestors.clear();
	
}
